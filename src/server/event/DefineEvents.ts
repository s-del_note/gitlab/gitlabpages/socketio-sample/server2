import SocketIO = require('socket.io');
import { EventFactory } from './factory/EventFactory';
import { User } from '../user/User';

export class DefineEvents {
  private readonly io: SocketIO.Server;
  private readonly users: Map<string, User>;

  constructor(io: SocketIO.Server, userMap: Map<string, User>) {
    this.io = io;
    this.users = userMap;
  }

  readonly define = (): void => {
    this.io.on('connection', (socket: SocketIO.Socket) => {
      const factory = new EventFactory(this.io, socket, this.users);
      factory.create('connection')?.listen();
      factory.create('send_message')?.listen();
      factory.create('change_name')?.listen();
      factory.create('disconnect')?.listen();
    });
  };
}
