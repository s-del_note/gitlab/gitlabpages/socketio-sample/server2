import SocketIO = require('socket.io');
import { User } from '../user/User';

export class ConnectionEvent implements IServerEvent {
  private readonly socket: SocketIO.Socket;
  private readonly users: Map<string, User>;

  constructor(socket: SocketIO.Socket, userMap: Map<string, User>) {
    this.socket = socket;
    this.users = userMap;
  }

  readonly listen = (): void => {
    console.log(`connection: ${this.socket.handshake.address}`);
    if (!this.socket.handshake.secure) {
      this.socket.emit('no_secure_alert', 'https で接続してください');
      this.socket.disconnect(true);
      return;
    }

    const newUser = new User(this.socket.handshake.address);
    this.users.set(this.socket.id, newUser);
    this.socket.emit('update_name', newUser.displayName());
  };
}
