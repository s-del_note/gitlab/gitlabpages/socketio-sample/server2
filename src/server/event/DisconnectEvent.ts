import SocketIO = require('socket.io');
import { User } from '../user/User';

export class DisconnectEvent implements IServerEvent {
  private readonly socket: SocketIO.Socket;
  private readonly users: Map<string, User>;
  private readonly eventName: RequestEventName;

  constructor(
    socket: SocketIO.Socket,
    userMap: Map<string, User>,
    eventName: RequestEventName
  ) {
    this.socket = socket;
    this.users = userMap;
    this.eventName = eventName;
  }

  readonly listen = (): void => {
    this.socket.on(this.eventName, () => {
      if (!this.users.has(this.socket.id)) return;
      console.log(`${this.socket.handshake.address} is disconnected`);

      this.users.delete(this.socket.id);
      this.socket.disconnect(true);
    });
  };
}
